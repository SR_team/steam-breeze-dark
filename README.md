 

# Steam Breeze Dark

Also available GNOME-like [CSD theme](https://gitlab.com/SR_team/steam-breeze-dark/-/tree/CSD)

### Base colors generated at [steamcustomizer](https://steamcustomizer.com/)

![Screenshot](README.assets/2021-10-04_02-44.png)
